# linkit_7688duo_uv_sensor_example

This project is to demonstrate the implementation of MQTT. **Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth, high latency and unreliable networks. The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication and plays and important role in the internet of things. MQTT works on the **TCP/IP connection**.

The Grove **UV Sensor** is used for detecting the intensity of incident ultraviolet(UV) radiation. This form of electromagnetic radiation has shorter wavelengths than visible radiation. The Sensor is based on the sensor GUVA-S12D which has a wide spectral range of 200nm-400nm.

**UV sensors** are used in many different applications. Examples include pharmaceuticals, automobiles, and robotics. UV sensors are also used in the printing industry for solvent handling and dyeing processes. In addition, UV sensors are also used in the chemical industry for the production, storage, and transportation of chemicals.[Click here to find more information about the UV Sensor.](http://wiki.seeedstudio.com/wiki/Grove_-_UV_Sensor)

## Overview


The library provides an example of publish and subscribe messaging with a server that supports MQTT using [Linkit Smart 7688 Duo.](https://docs.labs.mediatek.com/resource/linkit-smart-7688/en/tutorials/linkit-smart-7688-duo)
 
***Features provided by the client library:***

* Connect the device to any IP network using Ethernet, Wi-Fi, 4G/LTE
* Publish any message to the MQTT server in standard JSON format on a specific topic 
* Subscribe data from the server to the device on a specific topic
* Unsubscribe the topic to no longer communicate to the device
* Disconnect the device from any network connectivity.


***The following Table shows status of the client and the server when the above functions are implemented:***

> |Function |  Server Status   |    Client Status
> ----------------|------------------|---------------
|Looopmq.connect | Connected       | Connected|
|Loopmq.publish |Connected |  Connected|
|Loopmq.subscribe | Connected | Connected|
|Loopmq.unsubscribe | Connected | Disconnected|
|Loopmq.disconnect |  Disconnected |  Disconnected|


## Getting Started With Linkit 7688 Duo


**Basic steps to connect Linkit 7688 Duo to internet are given below:**

>1. Power up Linkit 7688 Duo using micro-USB cable
2. In your network manager, connect to the network with the name LinkIt_Smart_7688_XXXXXX  Access Point
3. Go to the web address http://mylinkit.local or ip `192.168.100.1`
4. Set a password with atleast six alphanumeric character and then submit. Enter the password again and click Sign in
5. Once logged in, Go to Networks tab,Select Station mode and enter the SSID and the password.
6. Click restart to configure the device and your Linkit 7688 Duo will be connected to the the internet via Wi-Fi

>**Note:**  
*To **factory restet** the device, hold the MPU and the Wi-Fi button together and then leave the MPU button when the Wi-Fi LED turns off, Press the Wi-Fi button for 20 secs and the board will be factory reset. 
*If you move to a different network and can no longer wirelessly access the Linkit 7688 Duo through its web interface, you can reset the network configuration by pressing the WiFi reset button (WLAN RST) for longer longer than 5 seconds but less than 20 secs,the WiFi configuration will be reset and the Linkit will start its own wiFi network LinkIt_Smart_7688_XXXXXX. Any other modification/configuration will be retained.*

**Steps to connect the board and then send data to MQTT are as below:**

>1. Assemble and connect the board with UV sensor connected on Analong pin A0
2. Install Arduino IDE and select Linkit 7688 Duo from boards and the COM port it is connected to
3. Install the library or open the *linkit_7688duo_uv_sensor_example.ino* file from the examples.
4. Enter the MQTT broker details in the *configuration.h* file present along with the *.ino* file.
5. Once code is compiled and flashed to yun, you should see the messages published by you to the device. The Led Status of the yun will be as shown in the figure 
![alt text](https://bytebucket.org/litmusloopdocs/arduino_linkit_7688duo/raw/master/extras/uv_setup.jpeg)


**Steps to test MQTT connection (if required) can be found under /repo/extras/testmqqt.md :**
***Note***: *If you are not using Google Chrome as your default browser, download **MQTTSpy** to test MQTT connection.*

## Configuration

The user need to define a list of parameters in order to connect the device to a server in a secured manner.

**Below are the list of minimum definitions required by the user to send data to the cloud:**

```
#define port_number 1883                                                                  // Port number
#define server "linkit7688duo.mqtt.litmus.pro"                                            // Server name
#define clientID "e5qobu85bi5xb9p3wdkoykh1k"                                              // DeviceID
#define password "rncqbhqth24atg953jvhaap3nr"                                             // Password
#define userID "e5qobu85bi5xb9p3wdkoykh1k"                                                // Username 
#define subTOPIC "loop/data/e5qobu85bi5xb9p3wdkoykh1k/elykqqiu3rousg3xo1z99bzxl/json"     // Subscribe on this topic to get the data
#define pubTopic "loop/data/e5qobu85bi5xb9p3wdkoykh1k/elykqqiu3rousg3xo1z99bzxl/json"     // Publish on this tpoic to send data or command to device 

```
## Functions

1.***loopmq.connect (client ID)***

This function is used to connect the device or the client to the client ID specified by the user.

```
if (loopmq.connect(c)) {
      Serial.println ("connected");
```       

2.***loopmq.connect (client ID, username, password)***

Checks for the username and password specified by the user to connect the device to the network.

```
if (loopmq.connect(c, user, pass))
  loopmq.publish(p,buffer);               // Publish message to the server once only
```

3.***loopmq.publish (topic, data)***

This function is used to publish data in string format to the topic specified by the user. 

```
Loopmq.publish (p,buffer);                // Publish message to the server
```

4.***loopmq.subscribe (topic)***

This function is used to subscribe to a topic to which data will be published from the user to the device. 

```
loopmq.subscribe(s);                       // Subscribe to a topic
```

5.***loopmq.unsubscribe (topic)***

This function is used to unsubscribe the device from the server. Calling this function will stop sending messages from the device to the server.

```
// loopmq.unsubscribe(s);                 // Note: uncomment the code to unsubscribe from the topic
```
 
6.***lopmq.disconnect ()***

This function is used to disconnect the device from the server. Disconnect does not stop the functionality of the device but disconnects it from the network. The device works fine locally but does not send any update to the internet.

```
// loopmq.disconnect();                   // Note: uncomment the code to disconnect the device
```

7.***loop ()***

This function is the sensor code for the Linkit 7688 duo.

```
  int sensorValue;
  long  sum=0;
  for(int i=0;i<1024;i++)                // accumulate readings for 1024 times
   {  
      sensorValue=analogRead(A0);
      sum=sensorValue+sum;
      delay(2);
   }   
 long meanVal = sum/1024;                 // get mean value
 float UV_index = (meanVal*1000/4.3-83)/21 ;
 Serial.print("The current UV index is:");
 Serial.print(UV_index);                  // get a detailed calculating expression for UV index in schematic files. 
 Serial.print("\n");
 delay(20); 

```

8.***JSON PARSER***

This function is used to create a JSON payload to be passed to the broker as payload. Please refer the [link](https://github.com/bblanchon/ArduinoJson/wiki/Compatibility-issues) for any compalibility issues.

```
  DynamicJsonBuffer jsonBuffer; 
  
  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["timestamp"] = 0;                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;

  JsonArray& data = root.createNestedArray("values");
  JsonObject& data1 = data.createNestedObject();
  data1["objectId"]= 3203;
  data1["instanceId"]= 0;                                // Add data["key"]= value
  data1["resourceId"]= 5650;
  data1["datatype"]= "float";
  data1["value"]= UV_index;
  
```