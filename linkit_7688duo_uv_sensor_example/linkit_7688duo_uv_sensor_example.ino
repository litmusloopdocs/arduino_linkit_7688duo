/*
 Litmus Loop
*/

/*
Inlcude all the library files 
*/
#include <loopmq.h>
#include <Bridge.h>
#include<BridgeClient.h>
#include <stdlib.h>
#include <stdio.h>
#include <ArduinoJson.h> 
#include "configuration.h"

/*
Passing the defined information to char
*/
int port = port_number;
char hostname[] = server;                     // Server name
char c[] = clientID;                        // ClientID
char pass[]= password;                      // password
char user[] = userID;                       // username
char p[]= subTOPIC;                           // Subscribe on this topic to get the data
char s[]= pubTopic;                           // Publish on this topic to send data or command to device 


BridgeClient yunClient;                       // yun client
PubSubClient loopmq(yunClient);               // instance to use the loopmq functions.

void callback(char* topic, byte* payload, unsigned int length) {
 //Serial.print("Message arrived [");
 //Serial.print(topic);
 //Serial.print("] ");
/*
Display the message published serially and on the LCD
*/
  for (int i=0;i<length;i++) {
    //Serial.print((char)payload[i]);           // Serial Display
  }
     //Serial.println();                         //Print on a new line
}

void reconnect() {                        
   
  while (!loopmq.connected()) {               // Loop until we're reconnected
  //Serial.print("Attempting MQTT connection...");    // Attempt to connect    
    if (loopmq.connect(c,user,pass)) {
    //Serial.println("connected");            // Once connected, publish an announcement...
         
         loopmq.subscribe(s);                 // Subscribe to a topic  
/*  
To Unscubscribe from a Topic uncomment the code below   
*/
        
   // loopmq.unsubscribe(s); 
   

      /*
To disconnect unocmment the code below
*/
      
   //  loopmq.disconnect();            
    } 
    else {
   //Serial.print("failed, rc=");
   //Serial.print(loopmq.state());
   //Serial.println(" try again in 5 seconds");
      
      delay(5000);                            // Wait 5 seconds before retrying
    }
  }
}

void setup(){
 /* 
   Bridge takes about two seconds to start up
   it can be helpful to use the on-board LED
   as an indicator for when it has initialized
  */
     
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  Bridge.begin();
  digitalWrite(13, HIGH);

  //Serial.begin(57600);                //Start the Serial Connection

  loopmq.setServer(hostname, port);           // Connect to the specified server and port defined by the user
  loopmq.setCallback(callback);               // Call the callbeck funciton when published   

  

  delay(1500);                                // Allow the hardware to sort itself out
}
 
void loop()
{  
  /*
  Sensor Code for UV sensor
  */
  int sensorValue;
  long  sum=0;
  for(int i=0;i<1024;i++)// accumulate readings for 1024 times
   {  
      sensorValue=analogRead(A0);
      sum=sensorValue+sum;
      delay(2);
   }   
 long meanVal = sum/1024;  // get mean value
 float UV_index = (meanVal*1000/4.3-83)/21 ;
// Serial.print("The current UV index is:");
// Serial.print(UV_index);// get a detailed calculating expression for UV index in schematic files. 
// Serial.print("\n");
 delay(20); 

 /*  
  JSON parser
  */
 /* 
  StaticJsonBuffer<200> jsonBuffer;               //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "UV sensor";                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;
 // root["number"]= var;

  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["UV value"]= UV_index;                                // Add data["key"]= value
 // data["LED Status"]=ledStatus;
*/
  DynamicJsonBuffer jsonBuffer; 
  
  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["timestamp"] = 0;                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;

  JsonArray& data = root.createNestedArray("values");
  JsonObject& data1 = data.createNestedObject();
  data1["objectId"]= 3203;
  data1["instanceId"]= 0;                                // Add data["key"]= value
  data1["resourceId"]= 5650;
  data1["datatype"]= "float";
  data1["value"]= UV_index;


  root.printTo(Serial);                           // prints to serial terminal
 // Serial.println();

  char buffer[600];                               // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));           // copy the JSON to the buffer to pass as a payload 
  
  /*
  Publish to the server
  */
  
  if (!loopmq.connected()) {
    reconnect();                              // Try to reconnect if connection dropped
  }
  if (loopmq.connect(c, user, pass)){
 
   loopmq.publish(p,buffer);                     // Publish message to the server once only
     delay(1000);
  }
  
  loopmq.loop();                              // check if the network is connected and also if the client is up and running
 
}
